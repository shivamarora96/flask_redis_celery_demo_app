## To run the server:

1. Activate virtual Env - source ./env/bin/activate
2. Command : python run.py

## To run the celery worker

1. Activate virtual env = source ./env/bin/activate
2. Command : celery -A app worker --loglevel=INFO

## To Run the Test cases

1. Activate Virtual env
2. Start the local server (Note the port should be 5000
3. Command : pytest 

