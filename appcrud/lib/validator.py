class Validator:

    def __init__(self):
        pass

    def validateName(self, name):
        if not name :
            raise ValueError('name is Empty')
        if type(name) is not str:
            raise ValueError('name param should be of type string')
