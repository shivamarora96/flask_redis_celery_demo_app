import urllib3, json

class HttpClient:
    __http = None
    
    def __createHttpClient(self):
        return urllib3.PoolManager()


    def __init__(self):
        HttpClient.__http = self.__createHttpClient()


    def get(self, url, f=None):
        try:
            r = HttpClient.__http.request('GET', url,fields=f)
            return r
        except Exception as identifier:
            print(identifier)
    

    def post(self, url, f=None):
        try:
            r = HttpClient.__http.request('POST', url, fields=f)
            return
        except Exception as identifier:
            print(identifier)


    def put(self, url, f=None):
        try:
            r = HttpClient.__http.request('PUT', url, fields=f)
            return r
        except Exception as identifier:
            print(identifier)        

    def convertBinaryToString(self, data):
        return data.decode('utf-8')