from model.student import Student, db
from flask import jsonify
from lib.custom_redis import CustomRedis
from lib.casync import reverseCallback,celery_app
from celery.result import AsyncResult
from lib.validator import Validator
import json

class ResponseData:

    """ READ  """

    @staticmethod
    def findByName(name):
        try:
            validator = Validator()
            validator.validateName(name)

            data = CustomRedis.get_tuple(name)
            if data:
                return jsonify({'res': data, 'isCached': True})
            
            data = Student.query.filter_by(name = name).all()
            
            if len(data)<1:
                return jsonify({'res': 'No Result found for '+ name})
        
            return jsonify({'res': data[0].getJSON(), 'isCached': False })
        
        except Exception as e:
            res = jsonify({'res': str(e)})
            res.status_code = 501    
            return res


    """ Update  """    
    
    @staticmethod
    def updateByName(currentName, newName):

        try:
            validator = Validator()
            validator.validateName(currentName)
            validator.validateName(newName)
            data = Student.query.filter_by(name = currentName).all()
            if(len(data)<1):
                return jsonify({'res': 'No Result found for '+ currentName})
    
            query = "Update Student set name='" + newName + "' where name='" + currentName + "'"
            db.session.execute(query)
            db.session.commit()

            cached_data = CustomRedis.get_tuple(currentName)
            if cached_data:
                CustomRedis.delete_tuple(currentName)
                CustomRedis.set_tuple(newName, cached_data)
        
            msg = currentName + ' updated to ' + newName
            return jsonify({'res':msg}) 

        except Exception as e:
            res =  jsonify({'res':e})
            res.status_code = 501
            return res
        



    """ Delete  """

    @staticmethod
    def deleteAll():
        db.session.execute('delete from Student')
        db.session.commit()
        CustomRedis.delete_all_tuple()
        return jsonify({'res': 'All records deleted!'})




    """ READ ALL  """


    @staticmethod
    def getAll():
        data = Student.query.all()
        res = [x.ser() for x in data]
        return jsonify({'res': res})



    """ Create """


    @staticmethod
    def addStudent(name):
        try:
            v = Validator()
            v.validateName(name)
            s = Student(name)
            db.session.add(s)
            db.session.commit()
            print('Creating Student: ' + name)
            CustomRedis.set_tuple(name,s.getJSON())
            return jsonify({'result': name + ' created Successfully!'})
        except Exception as identifier:
            res = jsonify({'res': str(identifier)})
            res.status_code = 501
            return res
            




    """ Async Reverse """


    @staticmethod
    def reverseAsync(name):
        res = reverseCallback.delay(name)
        return jsonify({'jobid' : res.id})





    """ Async Get Task using JOBID """

    @staticmethod
    def getJob(jobid):
        res = AsyncResult(jobid, app=celery_app)
        if res.status.lower() != 'success':
            return jsonify({'jobid': jobid,  'status': res.status, 'result': 'Task Failed !'})

        return jsonify({'jobid': jobid,  'status': res.status, 'result': res.get()})
    

