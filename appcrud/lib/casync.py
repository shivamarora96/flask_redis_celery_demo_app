from celery import Celery
# from celery.task import
import time

celery_app = Celery('app', broker='amqp://guest@localhost', backend='redis://localhost:6379') 

@celery_app.task(bind=True)
def reverseCallback(self, name, retry_kwargs={'max_retries': 50}):
    try:
        time.sleep(5)
        return name[::-1]
    except Exception as identifier:
        self.retry(exec=identifier, countdown=60)
    