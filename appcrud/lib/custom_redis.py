from redis import Redis
import json

class CustomRedis:
    __redis = None

    # print(' ***CustomRedis Called***  ')
    #constructor
    def __init__(self):
        if CustomRedis.__redis == None:
            CustomRedis.__redis = Redis('localhost')
        else:
            raise Exception("Already instance of this class is initiated")
                    
    @staticmethod
    def getInstance():
        if not CustomRedis.__redis:
            CustomRedis()
        return CustomRedis.__redis

    @staticmethod
    def set_tuple(key, data, ttl=60):
        # if CustomRedis.__redis == None:
            # raise Exception("Redis instance is empty!!")
        if not key or not data :
            raise Exception('Either Empty key or data !!')    
        if type(key) is not str:
            raise Exception("key is not a valid string")
        if type(data) is not dict:
            raise Exception("data provided should be dict")
        
        encode_data =  json.dumps(data)   
        return CustomRedis.getInstance().set(key, encode_data, ttl)       

    @staticmethod
    def get_tuple(key):
        # if CustomRedis.__redis == None:
            # raise Exception("Redis instance is empty!!")
        if not key :
            raise Exception('Either Empty key or data !!')    
        if type(key) is not str:
            raise Exception("key is not a valid string")
        x = CustomRedis.getInstance().get(key)
        if(x is not None):
            temp_data = json.loads(x.decode('utf-8'))
            return temp_data
        return x 

    @staticmethod
    def delete_tuple(key):
        # if CustomRedis.__redis == None:
            # raise Exception("Redis instance is empty!!")
        if not key :
            raise Exception('Either Empty key or data !!')    
        if type(key) is not str:
            raise Exception("key is not a valid string")
        return CustomRedis.getInstance().delete(key) 

    @staticmethod
    def delete_all_tuple():
        # if CustomRedis.__redis == None:
            # raise Exception("Redis instance is empty!!")
        return CustomRedis.getInstance().flushall() 
