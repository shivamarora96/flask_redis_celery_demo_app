from flask import Flask, jsonify, render_template
from appcrud.config import app_config
from appcrud.model.student import *

# from appcrud.view.route import set_route
from appcrud.lib.http_client import HttpClient
import pytest

def create_test_app(env):
    app = Flask(__name__)
    app.config.from_object(app_config['testing'])    
    db.init_app(app)
    
    with app.app_context():
        db.create_all()
    
    @app.route('/')
    def index():
        return "<h1>Welcome to app </h1>"
    app.run()


@pytest.fixture
def client():
    return HttpClient()

