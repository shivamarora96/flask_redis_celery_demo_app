import json

BASE_URL = 'http://localhost:5000'

""" Index """


def test_index(client, url='/'):
    current_url = BASE_URL + url
    response = client.get(current_url)
    data = client.convertBinaryToString(response.data)
    assert str(response.status)[0] == '2'
    assert  len(data) > 5   


""" GET ALL """


def test_get_all(client, url='/api/get/all'):
    
    current_url = BASE_URL + url
    response = client.get(current_url)
    data = client.convertBinaryToString(response.data)
    assert str(response.status)[0] == '2'
    assert type(json.loads(data)) == dict
    assert json.loads(data)['res']
    assert  type(json.loads((data))['res']) == list




""" GET BY NAME """


def test_get_name(client, url='/api/get/shivam'):
    current_url = BASE_URL + url
    response = client.get(current_url)
    data = client.convertBinaryToString(response.data)
    assert str(response.status)[0] == '2'
    assert type(json.loads(data)) == dict
    assert json.loads(data)['res'] 



""" UPDAtE BY  NAME """


def test_update_name(client, url='/api/update/shivam/shivam1'):
    current_url = BASE_URL + url
    response = client.put(current_url)
    data = client.convertBinaryToString(response.data)
    assert str(response.status)[0] == '2'
    assert type(json.loads(data)) == dict
    assert json.loads(data)['res'] 
    #Backtrack
    response = client.put(BASE_URL+'/api/update/shivam1/shivam')

