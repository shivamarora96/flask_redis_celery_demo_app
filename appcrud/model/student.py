from . import db
from flask import jsonify

class Student(db.Model):
   id = db.Column('student_id', db.Integer, primary_key = True)
   name = db.Column(db.String(100))
   city = db.Column(db.String(50))  
   addr = db.Column(db.String(200))
   pin = db.Column(db.String(10))

   def __init__(self, name):
    self.name = name
    self.city = 'Delhi'
    self.addr = 'Address'
    self.pin = 'Pincode' 

   def getJSON(self):
          return {
             'name': self.name,
             'Address': self.addr,
             'city': self.city,
             'pin': self.pin
          }

   def ser(self):
        return {
            'name': self.name,
            'Address': self.addr + self.city + self.pin
        }