from lib.response_data import ResponseData

def set_route(app, Student, db, jsonify):
   
 @app.route('/api/add/<string:name>', methods=['GET','POST'])
 def addStudent(name):
    return ResponseData.addStudent(name)


 @app.route('/api/get/all/', methods=['GET'])
 def getAll():
    return ResponseData.getAll()


 @app.route('/api/get/<string:name>', methods=['GET'])
 def findStudent(name):
   return ResponseData.findByName(name)   


 @app.route('/api/update/<string:name>/<string:new>', methods=['PUT', 'GET'])
 def update(name, new):
    return ResponseData.updateByName(name,new)


 @app.route('/api/delete/all', methods=['DELETE', 'GET'])
 def deleteall():
    return ResponseData.deleteAll()

 @app.route('/api/async/r/<string:name>')
 def reverseAsync(name):
    return ResponseData.reverseAsync(name)  

 @app.route('/api/async/<string:jobid>')
 def getJob(jobid):
    return ResponseData.getJob(jobid)     
