from flask import Flask, jsonify, render_template, request
from model.student import db, Student
from config import app_config
from view.route import set_route
from lib.casync import *


def create_app(env):
    app = Flask(__name__, static_folder='./templates/static')
    app.config.from_object(app_config[env])
    db.init_app(app)

    with app.app_context():
        db.create_all()

    @app.route('/')
    def index():
        # __data = request.get_json()
        # return str(__data)
        return render_template('index.html')

    set_route(app, Student, db, jsonify)

    return app
