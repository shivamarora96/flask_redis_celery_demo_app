import os

class Development():
    """
    Development environment configuration
    """

    DEBUG = True
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///student2.sqlite3'
    SQLALCHEMY_TRACK_MODIFICATIONS = True

class Production():
    """
    Production environment configurations
    """

    DEBUG = False
    TESTING = False
    #todo cahnge the URI for production
    SQLALCHEMY_DATABASE_URI = 'sqlite:///student2.sqlite3' 

class Testing():
    """
    Testing environment configurations
    """

    DEBUG = True
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///teststudent2.sqlite3' 
    
app_config = {
    'development': Development,
    'production': Production,
    'testing': Testing,
}